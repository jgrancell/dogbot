package command

import (
  "github.com/bwmarrin/discordgo"
)

func CmdHelp (s *discordgo.Session, m *discordgo.MessageCreate) {
  s.ChannelMessageSend(m.ChannelID, "**General Commands**\nhelp      -  Shows this help information.\ngames     - Shows the current games we have an organized, official presence in and how to join.\ngamenight - Gives information about the next upcoming Dog Fort games night.\n\n**DOGFT Eve Corp Commands**\nauth      - Provides the link to corp Auth.\nevetime   - Shows the current Eve Online in-game time.\nfittings  - Provides the link to corp fittings.\norders    - Gives the current DOGFT standing orders for subcaps/caps/supers (depending on channel used in).\n")
  return
}
// Current commands pre-smooshed

//**General Commands**\n
//help      -  Shows this help information.\n
//games     - Shows the current games we have an organized, official presence in and how to join.\n
//gamenight - Gives information about the next upcoming Dog Fort games night.\n
//\n
//**DOGFT Eve Corp Commands**\n
//auth      - Provides the link to corp Auth.\n
//evetime   - Shows the current Eve Online in-game time.\n
//fittings  - Provides the link to corp fittings.\n
//orders    - Gives the current DOGFT standing orders for subcaps/caps/supers (depending on channel used in).\n
