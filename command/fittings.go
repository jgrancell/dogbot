package command

import (
  "github.com/bwmarrin/discordgo"
)

func CmdFittingsLink (s *discordgo.Session, m *discordgo.MessageCreate) {
  var available bool

  switch m.ChannelID {
  case "261178897528258560": // Corp chat
    fallthrough
  case "142109395415859201": // Skew Crew
    fallthrough
  case "334033721844039680": // Supers
    fallthrough
  case "294486805544304640": // Uncle Touchey
    available = true
  default:
    available = false
  }

  if available {
    s.ChannelMessageSend(
      m.ChannelID,
      "Fittings can be found here:\n  - Subcapitals: https://wiki.dogft.com/en/doctrines\n  - Capitals: https://wiki.dogft.com/en/doctrines/capitals\n  - https://wiki.dogft.com/en/doctrines/super-capitals\n",
    )
    return
  }
  s.ChannelMessageSend(m.ChannelID, "This command can only be used in #corpfort and restricted cap/super channels.")
  return
}
