package command

import (
  "github.com/bwmarrin/discordgo"
)

func CmdAuthLink (s *discordgo.Session, m *discordgo.MessageCreate) {
  s.ChannelMessageSend(m.ChannelID, "The DOGFT Auth can be found at https://seat.dogft.com.")
  return
}
