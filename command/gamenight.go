package command

import (
  "github.com/bwmarrin/discordgo"
)

func CmdCommunityGameNight (s *discordgo.Session, m *discordgo.MessageCreate) {
  s.ChannelMessageSend(m.ChannelID, "The next Dog Fort Game night is scheduled for:\n  **Night:** June 14th at 10PM EST\n  **Game(s):** TBD")
  return
}
