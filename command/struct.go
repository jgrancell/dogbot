package command

type Command struct {
  Name          string
  Method        string
  Protected     bool
  ValidChannels []string
}
