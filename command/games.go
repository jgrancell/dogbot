package command

import (
  "github.com/bwmarrin/discordgo"
)

func CmdCommunityGames (s *discordgo.Session, m *discordgo.MessageCreate) {
  s.ChannelMessageSend(m.ChannelID, "Our current active games are:\n  - **Eve Online:** Message a recruiter here to join.\n")
  return
}
