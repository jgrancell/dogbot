package command

import (
  "github.com/bwmarrin/discordgo"
)

func CmdStandingOrders (s *discordgo.Session, m *discordgo.MessageCreate) {
  var available bool
  var capitals string
  var supercapitals string

  capitalOrders := "  - **Capitals:** Staging in M-O. Maintain DOGFT/BOOM fits for now.\n"
  supercapOrders := "  - **Supercaps:** Staging in M-O. Maintain DOGFT/BOOM fits for now.\n"

  switch m.ChannelID {
  case "261178897528258560": // Corp chat
    available = true
  case "142109395415859201": // Skew Crew
    available = true
    capitals = capitalOrders
  case "334033721844039680": // Supers
    available = true
    capitals = capitalOrders
    supercapitals = supercapOrders
  case "294486805544304640": // Uncle Touchey
    available = true
    capitals = capitalOrders
    supercapitals = supercapOrders
  default:
    available = false
  }

  if available {
    s.ChannelMessageSend(
      m.ChannelID,
      "Current corp standing orders are:\n  - **Staging System:** M-O Keepstar.\n  - **We Blappin'?:** Nah\n"+capitals+supercapitals,
    )
    return
  }
  s.ChannelMessageSend(m.ChannelID, "This command can only be used in #corpfort and restricted cap/super channels.")
  return
}
