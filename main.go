package main

import (
  "math/rand"
  "os"
  "os/signal"
  "strings"
  "syscall"

  subcom "gitlab.com/dogft/dogbot/command"
  log "gitlab.com/dogft/dogbot/logging"
  "github.com/bwmarrin/discordgo"
)

var prefix   string
var commands = map[string]interface{}{
  "help":      subcom.CmdHelp,
  "games":     subcom.CmdCommunityGames,
  "gamenight": subcom.CmdCommunityGameNight,
  "a valid bot command": subcom.CmdShitpost,

  "auth":     subcom.CmdAuthLink,
  "convert":  subcom.CmdTimeConvert,
  "evetime":  subcom.CmdTimeEve,
  "fittings": subcom.CmdFittingsLink,
  "orders":   subcom.CmdStandingOrders,


}

var invalids int = 0

func main () {
  var token string
  if token = os.Getenv("BOT_TOKEN"); token == "" {
    log.Error("Unable to load BOT_TOKEN environment variable.")
    os.Exit(1)
  }

  if prefix = os.Getenv("BOT_PREFIX"); prefix == "" {
    log.Error("Unable to load BOT_PREFIX environment variable, setting to '!'")
    prefix = "!"
  }

  dg, err := discordgo.New("Bot " + token)
  if err != nil {
    log.Error("Error creating discord client object: " + err.Error())
    os.Exit(1)
  }

  dg.AddHandler(messageCreate)

  if err = dg.Open(); err != nil {
    log.Error("Error opening connection to Discord servers: " + err.Error())
    os.Exit(1)
  }

  log.Info("Bot started successfully.")
  sc := make(chan os.Signal, 1)
  signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
  <-sc

  dg.Close()

}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
  // Ignoring all messages by this Bot
  if m.Author.ID == s.State.User.ID {
    return
  }

  // Ignoring all messages that don't include our prefix in position 0
  if ! strings.HasPrefix(m.Content, prefix) {
    return
  }

  num := rand.Intn(1000)

  if num == 211 || num == 311 || num == 411 {
    s.ChannelMessageSend(m.ChannelID, "What the fuck did you just fucking say about me, you little bitch? I’ll have you know I am a senior member of the Communist Politiburo, and I’ve been involved in numerous secret planning sessions against Cat Fort, and I have seized the means of production over 300 times. I am highly trained in ~poaching~ recruitment and I’m the top shitposter in the entire #zimdogwe channel. You are nothing to me but just another bourgeoisie scum. I will wipe our memes out with precision the likes of which has never been seen before on this Discord, mark my fucking words. You think you can get away with continuing to exist in Glorious Zimdogwe? Think again, fucker. As we speak I am contacting my secret network of canine enforcers across the USA and your shitty meme is being traced right now so you better prepare for the storm, scum. The storm that wipes out the pathetic little thing you call your posting. You’re fucking dead, kid. I can be anywhere, anytime, and have memorized over seven hundred image macros, and that’s just the ones you don't know. Not only am I extensively trained in shitposting, but I have access to the entire arsenal of Grables and Anchovies and I will use them to their full extent to wipe your miserable memes off the face of the internet, you little shit. If only you could have known what unholy retribution your little “clever” comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn’t, you didn’t, and now you’re paying the price, you goddamn idiot. I will shitpost fury all over you and you will drown in it. You’re fucking garbo, kiddo.")
    return
  } else if num == 169 {
    s.ChannelMessageSend(m.ChannelID, "I'm going to murder you in your sleep, parboil your skull, and use it as a candy dish for the holdays.")
    return
  } else if num == 1 {
    s.ChannelMessageSend(m.ChannelID, "I choo choo choose you to get kicked from the corp. @Ashkrall, sic him.")
  }

  cmd := strings.TrimPrefix(m.Content, prefix)

  if _, ok := commands[cmd]; ! ok {
    invalids++
    if invalids == 150 {
      s.ChannelMessageSend(m.ChannelID, "I'm going to murder you in your sleep, parboil your skull, and use it as a candy dish for the holdays.")
    } else if invalids % 20  == 0 {
      s.ChannelMessageSend(m.ChannelID, "Do you think that bot abuse is fun? Does it make you feel like a big strong man? Kiss my digital ass.")
      return
    }
    s.ChannelMessageSend(m.ChannelID, "No.")
    return
  }

  commands[cmd].(func(*discordgo.Session, *discordgo.MessageCreate))(s, m)
  return
}
