package logging

import (
  "fmt"
  "time"
)

func Info(message string) {

  log(message, "INFO")
}

func Error(message string) {
  log(message, "ERROR")
}

func log(message string, severity string) {
  currentTime := time.Now()
  timestamp := currentTime.Format("2006-01-02 15:04:05")
  fmt.Println(timestamp + " level=\""+severity+"\" message=\""+message+"\"")
}
